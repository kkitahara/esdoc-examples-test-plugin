v1.1.6
- Fix README.md.

v1.1.5
- Keep leading spaces in the test lines.

v1.1.4
- Add the line break at the final line again.

v1.1.3
- Minor change: output style
  - Remove the line break at the final line.

v1.1.2
- Add option `recursive: true` to fs.mkdir call.

v1.1.1
- Added CHANGELOG.md, .npmignore, CI.
- Change repository address.

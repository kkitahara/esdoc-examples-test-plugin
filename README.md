[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)
[![license](https://img.shields.io/npm/l/@kkitahara/esdoc-examples-test-plugin.svg)](https://www.apache.org/licenses/LICENSE-2.0)
[![pipeline status](https://gitlab.com/kkitahara/esdoc-examples-test-plugin/badges/v1.1.6/pipeline.svg)](https://gitlab.com/kkitahara/esdoc-examples-test-plugin/commits/v1.1.6)
[![version](https://img.shields.io/npm/v/@kkitahara/esdoc-examples-test-plugin/latest.svg)](https://www.npmjs.com/package/@kkitahara/esdoc-examples-test-plugin)
[![bundle size](https://img.shields.io/bundlephobia/min/@kkitahara/esdoc-examples-test-plugin.svg)](https://www.npmjs.com/package/@kkitahara/esdoc-examples-test-plugin)
[![downloads per week](https://img.shields.io/npm/dw/@kkitahara/esdoc-examples-test-plugin.svg)](https://www.npmjs.com/package/@kkitahara/esdoc-examples-test-plugin)
[![downloads per month](https://img.shields.io/npm/dm/@kkitahara/esdoc-examples-test-plugin.svg)](https://www.npmjs.com/package/@kkitahara/esdoc-examples-test-plugin)
[![downloads per year](https://img.shields.io/npm/dy/@kkitahara/esdoc-examples-test-plugin.svg)](https://www.npmjs.com/package/@kkitahara/esdoc-examples-test-plugin)
[![downloads total](https://img.shields.io/npm/dt/@kkitahara/esdoc-examples-test-plugin.svg)](https://www.npmjs.com/package/@kkitahara/esdoc-examples-test-plugin)

[![pipeline status](https://gitlab.com/kkitahara/esdoc-examples-test-plugin/badges/master/pipeline.svg)](https://gitlab.com/kkitahara/esdoc-examples-test-plugin/commits/master)
(master)

[![pipeline status](https://gitlab.com/kkitahara/esdoc-examples-test-plugin/badges/develop/pipeline.svg)](https://gitlab.com/kkitahara/esdoc-examples-test-plugin/commits/develop)
(develop)

# ESDoc Examples-Test Plugin

This plugin converts the contents of `@exmaple` blocks into test codes.

Currently, the plugin can generate only the codes that use ES6 modules.

## Instllation

```
npm install esdoc @kkitahara/esdoc-examples-test-plugin
```

## Configuration
The minimum configuration may like:
```
{
  "source": "./src",
  "destination": "./doc",
  "includes": ["\\.js$|\\.mjs$"],
  "plugins": [
    {
      "name": "@kkitahara/esdoc-examples-test",
      "option": {
        "path": "./examples-test"
      }
    }
  ]
}
```

Complete list of options:

| Name              | Default                                                            | Description                                                            |
|-------------------|--------------------------------------------------------------------|------------------------------------------------------------------------|
| path              | &mdash;                                                            | Output directory path. (required)                                      |
| suffix            | "-example"                                                         | Added to the name of each test code.                                   |
| mainOutput        | "run-test.mjs"                                                     | Name of the main test code.                                            |
| testDriverPath    | "@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs" | Test driver path.                                                      |
| quietPass         | false                                                              | If true, no log output for passed tests.                               |
| testDriverVarName | "testDriver"                                                       | Change it if the default conflicts with your example codes.            |
| replace           | []                                                                 | e.g. `[['a', 'b']]` will replace all `'a'` in the examples with `'b'`. |

## Example

Here, we consider that `path` is set at `./examples-test`, and other options are set at the default values.
Let's consider an `@example` block like:
```javascript
/**
 * @example
 * let a = 2
 * let b = 3
 *
 * // pass
 * a + b // 5
 *
 * // fail
 * a - b // 5
 *
 * // pass
 * a() // Error
 *
 * // fail
 * a + b // Error
 */
```
it will be converted into a test code like:
```javascript
import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

let a = 2
let b = 3

// pass
testDriver.test(() => { return a + b }, 5, 'my-awesome-example0_0', false)

// fail
testDriver.test(() => { return a - b }, 5, 'my-awesome-example0_1', false)

// pass
testDriver.test(() => { return a() }, Error, 'my-awesome-example0_2', false)

// fail
testDriver.test(() => { return a + b }, Error, 'my-awesome-example0_3', false)

```

What this plugin does is to detect the lines that match the pattern `A // B`.
If `A` is not empty, then, the line is converted to
a code like `testDriver.test(() => { return A }, B, 'my-awesome-example0_0')`.
Inside the `testDriver.test()` call,
the default test driver will assert the input values
by using the default `assert` module like
`assert.strictEqual(A, B)` if `B !== Error` and `assert.throws(() => { return A })` otherwise.

A test code will be produced per `@example` block.
To run all the tests, do
```
node --experimental-modules ./examples-test/run-test.mjs
```

## Coverage
The following may be used to get test-coverage report.
```
npx nyc --extension .mjs -i esm node ./examples-test/run-test.mjs
```
For this, you need [nyc](https://www.npmjs.com/package/nyc) and [esm](https://www.npmjs.com/package/esm).

## LICENSE
&copy; 2019 Koichi Kitahara  
[Apache 2.0](LICENSE)

import fs from 'fs'
import assert from 'assert'
import Converter from '../src/converter.js'

let converter = new Converter('../src/simple-test-driver.mjs', true)

let example = fs.readFileSync('./test/example.mjs').toString()
let testCode = converter.example2test(example, 'my-awesome-example')
let testCodeRef = fs.readFileSync('./test/test-quietpass-ref.mjs').toString()

assert.strictEqual(testCode, testCodeRef)

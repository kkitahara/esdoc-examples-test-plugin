import testDriver from '../src/simple-test-driver.mjs'

let a = 2
let b = 3

// pass
testDriver.test(() => { return a + b }, 5, 'my-awesome-example_0', true)

// fail
testDriver.test(() => { return a - b }, 5, 'my-awesome-example_1', true)

// pass
testDriver.test(() => { return a() }, Error, 'my-awesome-example_2', true)

// fail
testDriver.test(() => { return a + b }, Error, 'my-awesome-example_3', true)


let a = 2
let b = 3

// pass
a + b // 5

// fail
a - b // 5

// pass
a() // Error

// fail
a + b // Error

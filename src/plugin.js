/**
 * @source: https://www.npmjs.com/package/@kkitahara/esdoc-examples-test-plugin
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const fs = require('fs')
const Converter = require('./converter.js')

/**
 * @desc
 * The main body of @kkitahara/esdoc-examples-test-plugin.
 *
 * @version 1.0.0
 * @since 1.0.0
 */
class Plugin {
  /**
   * @desc
   * This method sets all the properties of `this` plugin.
   *
   * @param {Object} ev
   * passed by esdoc.
   *
   * @throws {Error}
   * if the `path` option is not set.
   *
   * @version 1.1.0
   * @since 1.0.0
   */
  onHandlePlugins (ev) {
    const {
      path,
      suffix = '-example',
      mainOutput = 'run-test.mjs',
      testDriverPath = '@kkitahara/esdoc-examples-test-plugin' +
          '/src/simple-test-driver.mjs',
      quietPass = false,
      testDriverVarName = 'testDriver',
      replace = [] } = ev.data.option
    if (!path) {
      throw Error('the `path` option is not set')
    }
    /**
     * @desc
     * All the test codes are written in this `path`.
     *
     * @type {string}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.path = path
    /**
     * @desc
     * It is added to the name of each test code.
     *
     * @type {string}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.suffix = suffix
    /**
     * @desc
     * The name of the main test code.
     *
     * @type {string}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.mainOutput = mainOutput
    this.converter = new Converter(testDriverPath, quietPass, testDriverVarName,
      replace)
    if (!fs.existsSync(path)) {
      fs.mkdirSync(path, { recursive: true })
    }
  }

  /**
   * @desc
   * This method creates the header of the main test code.
   *
   * @param {Object} ev
   * passed by esdoc.
   *
   * @version 1.0.0
   * @since 1.0.0
   */
  onStart (ev) {
    const path = this.converter.testDriverPath
    const varName = this.converter.testDriverVarName
    this.s = 'import ' + varName + ' from \'' + path + '\'\n\n'
  }

  /**
   * @desc
   * This method creates all the unit tests.
   * The test codes are written in separate files.
   *
   * @param {Object} ev
   * passed by esdoc.
   *
   * @version 1.0.0
   * @since 1.0.0
   */
  onHandleDocs (ev) {
    ev.data.docs.forEach((doc) => {
      if (doc.hasOwnProperty('examples')) {
        const nameBase = doc.longname + this.suffix
        for (let i = 0, n = doc.examples.length; i < n; i += 1) {
          const id = i.toString()
          const name = nameBase + id
          const fileName = name.replace(/[[\]/~#]/g, '_')
          const modulePath = './' + fileName + '.mjs'
          const filePath = this.path + '/' + fileName + '.mjs'
          this.s += 'import \'' + modulePath + '\'\n'
          fs.writeFileSync(
            filePath, this.converter.example2test(doc.examples[i], name))
        }
      }
    })
  }

  /**
   * @desc
   * This method creates the footer of the main test code
   * and write the code.
   *
   * @param {Object} ev
   * passed by esdoc.
   *
   * @version 1.0.0
   * @since 1.0.0
   */
  onComplete (ev) {
    this.s += '\n'
    this.s += 'testDriver.showSummary()\n'
    this.s += 'if (testDriver.fail) {\n'
    this.s += '  process.exit(1)\n'
    this.s += '}\n'
    const fileName = this.path + '/' + this.mainOutput
    fs.writeFileSync(fileName, this.s)
    console.log('examples-test: output test codes in \'' + this.path +
        '\'. To run the test, do')
    console.log('')
    console.log('  node --experimental-modules ' + fileName)
    console.log('')
  }
}

module.exports = new Plugin()

/* @license-end */

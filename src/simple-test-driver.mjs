/**
 * @source: https://www.npmjs.com/package/@kkitahara/esdoc-examples-test-plugin
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import assert from 'assert'
import colors from 'colors/safe'

/**
 * @desc
 * The defalt testdriver for @kkitahara/esdoc-examples-test-plugin.
 *
 * @version 1.0.0
 * @since 1.0.0
 */
class SimpleTestDriver {
  /**
   * @desc
   * The constructor function of the SimpleTestDriver class.
   *
   * @version 1.0.0
   * @since 1.0.0
   */
  constructor () {
    /**
     * @desc
     * The number of the successfully passed tests.
     *
     * @type {Number}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.pass = 0
    /**
     * @desc
     * The number of the failed tests.
     *
     * @type {Number}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.fail = 0
  }

  /**
   * @desc
   * This method is called for each test.
   *
   * @param {Function} testFunc
   * a function to be tested.
   *
   * @param {Object} expected
   * the expected result for the `testFunc`.
   *
   * @param {Object} testName
   * name of the test. Just for the output log.
   *
   * @param {boolean} quietPass
   * no log output if the test passes and `quietPass` is `true`.
   *
   * @version 1.0.0
   * @since 1.0.0
   */
  test (testFunc, expected, testName = '', quietPass = false) {
    let flag = true
    try {
      if (expected === Error) {
        assert.throws(testFunc)
      } else {
        assert.strictEqual(testFunc(), expected)
      }
    } catch (e) {
      flag = false
      this.fail += 1
      console.log(colors.red('FAIL: ') + testName)
      console.log(colors.red('='.repeat(80)))
      console.log(e.name + ': ' + e.message)
      console.log(colors.red('='.repeat(80)))
    }
    if (flag) {
      this.pass += 1
      if (!quietPass) {
        console.log(colors.green('PASS: ') + testName)
      }
    }
  }

  /**
   * @desc
   * This method is called at the end of the tests.
   *
   * @version 1.0.0
   * @since 1.0.0
   */
  showSummary () {
    console.log()
    console.log(colors.underline('Summary'))
    console.log('- ' + colors.green('PASS: ') + this.pass)
    console.log('- ' + colors.red('FAIL: ') + this.fail)
  }
}

const testDriver = new SimpleTestDriver()
export default testDriver

/* @license-end */

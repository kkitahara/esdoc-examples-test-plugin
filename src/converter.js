/**
 * @source: https://www.npmjs.com/package/@kkitahara/esdoc-examples-test-plugin
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @desc
 * The defalt converter for @kkitahara/esdoc-examples-test-plugin.
 *
 * @version
 * 1.0.0
 *
 * @since
 * 1.0.0
 */
class Converter {
  /**
   * @desc
   * The constructor function of the Converter class.
   *
   * @param {string} testDriverPath
   * assigned to `this.testDriverPath`.
   *
   * @param {boolean} [quietPass = false]
   * assigned to `this.quietPass`.
   *
   * @param {string} [testDriverVarName = 'testDriver']
   * assigned to `this.testDriverVarName`.
   *
   * @param {Array} [replace = []]
   * assigned to `this.replace`.
   *
   * @version 1.1.0
   * @since 1.0.0
   */
  constructor (
    testDriverPath,
    quietPass = false,
    testDriverVarName = 'testDriver',
    replace = []) {
    /**
     * @desc
     * path to a test-driver module.
     *
     * @type {string}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.testDriverPath = testDriverPath
    /**
     * @desc
     * no log output for the successfull tests if it is `true`.
     *
     * @type {boolean}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.quietPass = quietPass
    /**
     * @desc
     * use it if the default variable name conflicts with your test codes.
     *
     * @type {string}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.testDriverVarName = testDriverVarName
    /**
     * @desc
     * It is used to replace texts in codes.
     *
     * @type {Array}
     *
     * @version 1.1.0
     * @since 1.1.0
     */
    this.replace = replace
  }

  /**
   * @desc
   * This method converts an example code
   * into a test code.
   *
   * @param {string} example
   * example code.
   *
   * @param {string} testNameBase
   * name of the example code. Just for the output log.
   *
   * @return {string}
   * a test code.
   *
   * @version 1.1.5
   * @since 1.0.0
   */
  example2test (example, testNameBase) {
    const path = this.testDriverPath
    const quietPass = this.quietPass
    const varName = this.testDriverVarName
    const lines = example.split(/\r\n|\r|\n/)
    for (let i = lines.length - 1; i >= 0; i -= 1) {
      for (let j = 0, n = this.replace.length; j < n; j += 1) {
        const repj = this.replace[j]
        lines[i] = lines[i].replace(repj[0], repj[1])
      }
    }
    let s = ''
    let id = 0
    for (let i = 0, n = lines.length; i < n; i += 1) {
      const line = lines[i]
      const split = line.split(/\s*\/\/\s*/)
      if (split.length === 2 && split[0].length > 0) {
        const testName = testNameBase + '_' + id.toString()
        const [, leadWSpace, main] = /(\s*)(.*)/.exec(split[0])
        s += leadWSpace + varName + '.test(() => { return ' + main + ' }, ' +
          split[1] + ', \'' + testName + '\', ' + quietPass + ')' + '\n'
        id += 1
      } else {
        s += line + '\n'
      }
    }
    if (id !== 0) {
      s = 'import ' + varName + ' from \'' + path + '\'\n\n' + s
    }
    return s
  }
}

module.exports = Converter

/* @license-end */
